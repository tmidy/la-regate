export interface NotesState {
  notes: string[];
}

const initialState = {
  notes: [],
};

export type NoteTypeAction = "ADD_NOTE";

export type NoteAction = { type: NoteTypeAction; payload: string };

export const notesReducer = (
  state: NotesState = initialState,
  action: NoteAction
) => {
  switch (action.type) {
    case "ADD_NOTE": {
        console.log("je suis passé là")
      return { ...state, notes: [...state.notes, action.payload] };
    }
    default:
      return state;
  }
};
