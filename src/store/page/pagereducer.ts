import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

export type Page = "HOME" | "RESTAURANT" | "HOTEL" | "CONTACT";
export type SubPage = "SEAFOOD" | "BASEMENU"

export type PageTypeAction = "UPDATE_PAGE";

export interface PagesState {
  currentPage: Page;
  subPage?: SubPage;
}

const initialState: PagesState = {
  currentPage: "HOME",
};

export type PageAction = {
  currentPage: Page;
  subPage?: SubPage;
};


export const pageSlice = createSlice({
  name: 'page',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    updatePage: (state, action: PayloadAction<PageAction>) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      state.currentPage = action.payload.currentPage;
      state.subPage = action.payload.subPage;
    },
  },
});

export const { updatePage} = pageSlice.actions;

export default pageSlice.reducer;

export const selectPage = (state: RootState) => state.page;