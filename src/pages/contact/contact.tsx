import React from "react";
import { DatePick } from "../../components/common/datepick";

const Contact = () => {
  return (
    <div className="flex flex-col md:flex-row bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 h-full">
      <div className="w-full h-full">
        <form>
          <div className="mb-4 w-1/2">
            <label className="block text-gray-700 text-sm font-bold mb-2">
              Nom prénom
            </label>
            <input
              className="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="username"
              type="text"
              placeholder="Username"
            />
          </div>
          <div className="mb-4 w-1/2">
            <label className="block text-gray-700 text-sm font-bold mb-2">
              Ville
            </label>
            <input
              className="shadow appearance-none border py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              id="city"
              type="text"
            />
          </div>
          <div className="mb-4 w-1/2">
            <label className="block text-gray-700 text-sm font-bold mb-2">
              Téléphone
            </label>
            <input
              className="shadow appearance-none border py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              id="phone"
              name="phone"
              required
              pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
            />
          </div>
          <div className="mb-4 w-1/2">
            <label className="block text-gray-700 text-sm font-bold mb-2">
              Email
            </label>
            <input
              className="shadow appearance-none border py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              id="mail"
              name="mail"
              required
              pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2, 4}$"
            />
          </div>
          <div className="mb-4 w-full">
            <label className="block text-gray-700 text-sm font-bold mb-2">
              Dates de réservation
            </label>
            <div className="flex flex-row w-full">
              <div className="flex w-full">
                <label className="block text-gray-700 text-sm font-bold w-full p-1 ">
                  Début
                </label>
                <DatePick />
              </div>
              <div className="flex w-full">
                <label className="block text-gray-700 text-sm font-bold w-full p-1">
                  Fin
                </label>
                <DatePick />
              </div>
            </div>
          </div>

          <button
            onClick={() => {}}
            className="py-2 px-4 bg-blue-500 text-white font-semibold rounded-lg shadow-md hover:bg-white hover:text-red-500 focus:outline-none"
          >
            Envoyer
          </button>
        </form>
      </div>
      <div className="w-full min-h-full ">
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2664.4824832385375!2d-4.354901584159883!3d48.10092766169147!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4816d09765c89485%3A0xe5c231eb680af910!2sH%C3%B4tel%20des%20Sables%20Blancs%20Restaurant%20La%20Regate!5e0!3m2!1sfr!2sfr!4v1629468048728!5m2!1sfr!2sfr"
          className="w-full min-h-full"
          loading="eager"
          title="map"
        ></iframe>
      </div>
    </div>
  );
};

export default Contact;
