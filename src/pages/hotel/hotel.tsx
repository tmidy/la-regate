import React from "react";
import pricesAdpater, {
  filterNights,
  NightStandRecord,
  pricesKeys
} from "../../app/adapters/json";
import i18next from  "../../store/i18n/i18n";

const Hotel = () => {
  const buildTdPrices = (field: pricesKeys) => {
    return (
      <>
        <td className="border border-light-blue-500 text-light-blue-600 font-medium whitespace-nowrap paragraph">
          {field}
        </td>
        {Object.keys(pricesAdpater).map((key: string, idx: number) => {
          return (
            <td
              key={key + idx}
              className="border border-light-blue-500 px-4 py-2 text-light-blue-600 paragraph"
            >
              {pricesAdpater[key][field]}
            </td>
          );
        })}
      </>
    );
  };

  const buildTdWeekend = () => {
    const oneNights = filterNights("oneNight");
    const twoNights = filterNights("twoNights");
    return (
      <>
        <tr key="one-night-single">
          <td className="border border-light-blue-500 px-4 py-2 text-light-blue-600 paragraph">
            one-night-single
            <p>{oneNights[0].single.comments}</p>
          </td>
          {oneNights.map((value: NightStandRecord, idx: number) => {
            return (
              <td key={"one-night-single" + idx}>
                {oneNights[idx].single.bedroom}
              </td>
            );
          })}
        </tr>
        <tr key="one-night-couple">
          <td className="border border-light-blue-500 px-4 py-2 text-light-blue-600 paragraph ">
            <p className="font-bold">one-night-Couple</p>
            <p>{oneNights[0].couples.comments}</p>
          </td>
          {oneNights.map((value: NightStandRecord, idx: number) => {
            return (
              <td key={"one-night-couple" + idx}>
                {oneNights[idx].couples.bedroom}
              </td>
            );
          })}
        </tr>
        <tr key="two-night-Single">
          <td className="border border-light-blue-500 px-4 py-2 text-light-blue-600 paragraph">
            two-night-Single
            <div className="text-center">
              {twoNights[0].single.comments.split("\n").map((str) => (
                <p>{str}</p>
              ))}
            </div>
          </td>
          {twoNights.map((value: NightStandRecord, idx: number) => {
            return (
              <td key={"two-night-single" + idx}>
                {twoNights[idx].single.bedroom}
              </td>
            );
          })}
        </tr>
        <tr key="two-night-couple">
          <td className="border border-light-blue-500 px-4 py-2 text-light-blue-600 paragraph">
            <strong>two-night-couple</strong>
            <p>{twoNights[0].couples.comments}</p>
          </td>
          {twoNights.map((value: NightStandRecord, idx: number) => {
            return (
              <td key={"two-Night-couple" + idx}>
                {twoNights[idx].couples.bedroom}
              </td>
            );
          })}
        </tr>
      </>
    );
  };

  return (
    <div className="flex flex-col w-full h-full flex-gap⁻2 divide-y-2">
      <div className="title-h1 tracking-wide font-semibold italic w-full text-center p-4">
        Nos tarifs
      </div>
      <div className="w-full flex flex-col md:flex-row justify-center p-4">
        <table className="shadow-lg w-full">
          <thead>
            <tr>
              <th className="bg-blue-100 border text-left"> {i18next.t("stay")}</th>
              {Object.keys(pricesAdpater).map((key: string) => {
                return (
                  <th key={key} className="bg-blue-100 border text-left">
                    {key}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody className="bg-white divide-y divide-gray-200">
            <tr>{buildTdPrices("bedroom")}</tr>
            <tr>{buildTdPrices("breakfast")}</tr>
            <tr>{buildTdPrices("halfBoardSingle")}</tr>
            <tr>{buildTdPrices("halfBoardCouples")}</tr>
          </tbody>
        </table>
        <div className="w-full flex flex-col justify-center">
          <p className=" paragraph leading-6 p-4">
            Lit supplémentaire : {pricesAdpater["april-october"].extraBed} €
          </p>
          <p className=" paragraph leading-6 p-4 ">
            Animaux : {pricesAdpater["april-october"].pets} €
          </p>
        </div>
      </div>
      <div className="flex flex-col h-full">
        <div className="title-h1 tracking-wide font-semibold italic w-full text-center p-4">
          Weekend découvertes
          <p className="paragraph italic">
            {" "}
            Champagne, Plateau de fruits de mer et Thalasso
          </p>
        </div>
        <div className="w-full flex flex-col md:flex-row justify-center p-4">
          <table className="shadow-lg w-full">
            <thead>
              <tr>
                <th className="bg-blue-100 border text-left">Séjour</th>
                {Object.keys(pricesAdpater).map((key: string) => {
                  return (
                    <th key={key} className="bg-blue-100 border text-left">
                      {key}
                    </th>
                  );
                })}
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-200">
              {buildTdWeekend()}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Hotel;
