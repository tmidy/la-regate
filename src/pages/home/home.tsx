import React from "react";

const Home = () => {
  return (
    <div className="w-full h-full flex">
      <div className="text-high h-full">
        <div className="flex flex-col rounded-xl shadow-md md:overflow-hidden overflow-auto h-full divide-y-2">
          <div className="flex flex-col h-full">
            <div className="h-full title-h1 tracking-wide font-semibold italic w-full text-center p-4">
              HOTEL DES SABLES BLANCS
            </div>
            <div className="flex flex-col w-full h-full ">
              <div className="w-full flex">
                <img
                  src={
                    "http://www.hoteldessablesblancs.fr/wa_0_p/pa_15ixvshre48gyz/sl_cote_20gauche.jpg?1g3ydk2ral8gih"
                  }
                  className="App-logo w-full object-contain p-8 h-48"
                  alt="logo"
                />
              </div>
              <div className="w-full flex justify-center">
                <p className="text-gray-500 paragraph leading-6 p-4 w-2/3">
                  Situé à 80 mètres de la plage des Sables Blancs, du gr34, 800
                  mètres du centre de rééducation fonctionnelle de tréboul et à
                  150 mètres de la Thalasso de Douarnenez, Pierre, Magalie et
                  leur réquipe vous reçoivent tout au long de l'année dans un
                  cadre famillial.
                </p>
              </div>
            </div>
          </div>
          <div className="flex flex-col h-full">
            <div className="title-h1 tracking-wide font-semibold italic w-full text-center p-4">
              RESTAURANT LA REGATE
            </div>

            <div className="flex flex-col w-full h-full ">
              <div className="w-full flex">
                <img
                  src={
                    "http://www.hoteldessablesblancs.fr/wa_0_p/pa_15ixvshre48gyz/sl_cote_20gauche.jpg?1g3ydk2ral8gih"
                  }
                  className="App-logo w-full object-contain p-8 h-48"
                  alt="logo"
                />
              </div>
            </div>
            <div className="w-full flex justify-center">
              <p className="text-gray-500 paragraph leading-6 p-4 w-2/3">
                Nous vous proposons tout au long de l'année une restauration à
                partir de "Produits frais et Fait maison". Les Midis du lundi au
                vendredi un Menu du jour à 14 € et Formule à 12€ Plateaux de
                fruits de mer à l'année Duo ou Solo
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
