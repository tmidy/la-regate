import React, { useState } from "react";
import { PlateDescription, RestaurantAdapter } from "../../app/adapters/json";
import { SubPage } from "../../store/page/pagereducer";

const CardMenu = (): JSX.Element => {
  const entrees = RestaurantAdapter.baseMenu.starter;
  const plats = RestaurantAdapter.baseMenu.main;
  const desserts = RestaurantAdapter.baseMenu.dessert;
  return (
    <div className="w-full h-full flex flex-col divide-y-2 p-4">
      <div className="w-full h-full ">
        <div className="title-h2 tracking-wide font-semibold italic text-center">
          Nos Entrées
        </div>
        {entrees.map((plat: PlateDescription) => {
          return (
            <div key={plat.name} className="w-full flex">
              <div className="w-2/3">{plat.name}</div>
              <div className="w-1/3 text-center"> {plat.price}€</div>
            </div>
          );
        })}
      </div>
      {/* 2eme colonne */}
      <div className="w-full h-full">
        <div className="title-h2 tracking-wide font-semibold italic text-center">
          Nos Plats
        </div>

        <div className="flex flex-col w-full h-full">
          <div className="mb-2">
            <h3 className="font-semibold italic">la mer</h3>
            {plats.seaFood.map((plat: PlateDescription) => {
              return (
                <div key={plat.name} className="w-full flex">
                  <div className="w-2/3">{plat.name}</div>
                  <div className="w-1/3 text-center"> {plat.price}€</div>
                </div>
              );
            })}
          </div>
          <div className="mb-2">
            <h3 className="font-semibold italic">les viandes</h3>
            {plats.meatFood.map((plat: PlateDescription) => {
              return (
                <div key={plat.name} className="w-full flex">
                  <div className="w-2/3">{plat.name}</div>
                  <div className="w-1/3 text-center"> {plat.price}€</div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
      {/* 3eme colonne */}
      <div className="w-full h-full">
        <div className="title-h2 tracking-wide font-semibold italic text-center">
          Nos Desserts
        </div>
        {desserts.map((plat: PlateDescription) => {
          return (
            <div key={plat.name} className="w-full flex">
              <div className="w-2/3">{plat.name}</div>
              <div className="w-1/3 text-center"> {plat.price}€</div>
            </div>
          );
        })}
      </div>
      {/* 4eme colonne */}
      <div className="w-full h-full">
        <div className="title-h2 tracking-wide font-semibold italic text-center">
          Spécialité à la carte hors menu
        </div>
        <div className="w-full flex">
          <div className="w-2/3">
            Homard Breton Grillé (Entier corps et pinces) aux légumes croquants
            et sa purée Fumée Maison
          </div>
          <div className="w-1/3 text-center"> 54€</div>
        </div>
      </div>
    </div>
  );
};

const MenuOfTheDay = (): JSX.Element => {
  return (
    <div className="w-full h-full flex flex-col gap-16 p-4">
      <div className="w-full">
        <div className="title-h2 tracking-wide font-semibold italic text-center">
          MENU DU JOUR A 14 EUROS
          <p className="paragraph italic">
            Du lundi au vendredi uniquement le midi (Hors jours fériés et ponts)
          </p>
        </div>
        <p className="paragraph m-4 text-center">
          Entrée du Jour+Plat du Jour+ Dessert du Jour 1/4 De Limonade ou Vin +
          Café
        </p>
        <p className="paragraph m-4 text-center">Ou</p>
        <p className="paragraph m-4 text-center">
          Steak Frites Salade+Dessert du Jour 1/4 De Limonade Ou Vin + Café
        </p>
      </div>
      <div className="w-full">
        <div className="title-h2 tracking-wide font-semibold italic text-center">
          FORMULE DU JOUR 12 EUROS
          <p className="paragraph italic">
            Du lundi au vendredi uniquement le midi (Hors jours fériés et ponts)
          </p>
        </div>
        <p className="paragraph m-4 text-center">
          Entrée du Jour+Plat du Jour 1/4 De Limonade Ou Vin + Café
        </p>
        <p className="paragraph m-4 text-center">Ou</p>
        <p className="paragraph m-4 text-center">
          Plat du Jour+Dessert du Jour 1/4 De Limonade Ou Vin + Café
        </p>
        <p className="paragraph m-4 text-center">Ou</p>
        <p className="paragraph m-4 text-center">
          Steak Frites Salade 1/4 De Limonade Ou Vin + Café
        </p>
        <p className="paragraph italic m-4 pt-4 font-bold text-center">
          Note : Présence de produits industriels dans le menu du jour
        </p>
      </div>
    </div>
  );
};

const MobileMenu = (): JSX.Element => {
  return (
    <div className="w-full flex flex-row justify-center gap-4">
      <button className="py-2 px-4 bg-red-500 text-white font-semibold rounded-lg shadow-md hover:bg-white hover:text-red-500 focus:outline-none">
        La Carte
      </button>
      <button className="py-2 px-4 bg-red-500 text-white font-semibold rounded-lg shadow-md hover:bg-white hover:text-red-500 focus:outline-none">
        Formule du Jour
      </button>
    </div>
  );
};

const mappingMobile: Record<string, JSX.Element> = {
  cardMenu: CardMenu(),
  MenuOfTheDay: MenuOfTheDay(),
};

const BaseMenu = (): JSX.Element => {
  const [mobileDisplay, setMobileDisplay] = useState("cardMenu");

  return (
    <>
      <div className="flex flex-col w-full h-full">
        {window.innerWidth < 1000 && <MobileMenu />}

        {window.innerWidth < 1000 ? (
          <div className="w-full flex flex-row">
            {mappingMobile[mobileDisplay]}
          </div>
        ) : (
          <div className="w-full flex flex-row">
            <CardMenu />
            <MenuOfTheDay />
          </div>
        )}
      </div>
    </>
  );
};

const SeaFoodMenu = (): JSX.Element => {
  const soloDuos = RestaurantAdapter.seaFoodMenu.soloDuos;
  return (
    <>
      <div className="grid grid-cols-2 gap-4 w-full h-full">
        {soloDuos.map((plat: PlateDescription) => {
          return (
            <div
              key={plat.name}
              className="justify-center mx-auto bg-white rounded-xl shadow-md overflow-hidden w-full"
            >
              <div className="flex w-full">
                <div className="p-8 w-full">
                  <div className="title-h2 tracking-wide font-semibold italic text-center">
                    {plat.name}
                  </div>
                  <p className="mt-2 text-gray-500 text-center">
                    {plat.description?.split("\n").map(function (item, idx) {
                      return (
                        <span key={idx}>
                          {item}
                          <br />
                        </span>
                      );
                    })}
                  </p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

const Restaurant = ({ subPage }: { subPage: SubPage }): JSX.Element => {
  console.log(subPage);
  return (
    <div className="w-full h-full flex">
      {subPage ? (
        subPage === "BASEMENU" ? (
          <BaseMenu />
        ) : (
          <SeaFoodMenu />
        )
      ) : (
        <BaseMenu />
      )}
    </div>
  );
};

export default Restaurant;
