import Contact from "../pages/contact/contact";
import Home from "../pages/home/home";
import Hotel from "../pages/hotel/hotel";
import Restaurant from "../pages/restaurant/restaurant";
import { PagesState, SubPage } from "../store/page/pagereducer";

export const routeTo = (page: PagesState): JSX.Element => {
  switch (page.currentPage) {
    case "RESTAURANT":
      return Restaurant({ subPage: page.subPage as SubPage });
    case "HOME":
      return Home();
    case "CONTACT":
      return Contact();
    case "HOTEL":
      return Hotel();
  }
};
