import React from "react";
import "./App.css";
import { useAppSelector } from "./app/hooks";
import Footer from "./components/footer/footer";
import Header from "./components/header/header";
import { routeTo } from "./routes/route";
import { selectPage } from "./store/page/pagereducer";

function App() {
  const page = useAppSelector(selectPage);
  let comp: JSX.Element;
  if (page === undefined || page.currentPage === undefined) {
    comp = routeTo({ currentPage: "HOME" });
  } else {
    comp = routeTo(page);
  }

  return (
    <div className="container mx-auto max-h-screen">
      <div className="flex flex-col h-full">
        <Header />
        <div className="w-full h-full overflow-y">{comp}</div>
        <Footer />
      </div>
    </div>
  );
}

export default App;
