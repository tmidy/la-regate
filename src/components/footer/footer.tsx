import React from "react";
import i18next from  "../../store/i18n/i18n";

const Footer = () => {
  return (
    <>
      <footer className="footer relative lg:absolute w-full md:w-1/2">
        <div className="px-6 sm:flex w-full justify-center">
          <div className="sm:flex">
            <div className="font-bold text-gray-700 uppercase mb-2">
              <span className="mx-2">{i18next.t("legalmentions")}</span>
              <span className="mx-2">Contact</span>
            </div>
          </div>
        </div>
        <div className="mx-auto px-6">
          <div className="mt-2 border-t-2 border-gray-300 flex flex-col items-center">
            <div className="sm:w-2/3 text-center">
              <p className="text-sm text-blue-200 font-bold">
                © 2021. Restaurant La Régate
              </p>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
