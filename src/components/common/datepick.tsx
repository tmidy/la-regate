import React from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export const DatePick = () => {
  return (
      <DatePicker
        selected={new Date()}
        onChange={(date: Date) =>{}}
        selectsStart
        startDate={new Date()}
        minDate={new Date()}
        className="w-1/2 shadow appearance-none border"
        nextMonthButtonLabel=">"
        previousMonthButtonLabel="<"
      />
  );
};
