import { faEnvelopeOpenText, faHome, faHotel, faUtensils } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { useAppDispatch } from "../../app/hooks";
import { updatePage } from "../../store/page/pagereducer";

const Header = () => {
  const dispatch = useAppDispatch();

  function handleResize(): string {
    if (window.innerWidth > 1000) {
      return "writing";
    }
    return "icon";
  }

  const [resizeState, setResizeState] = useState(handleResize());

  React.useEffect(() => {
    function handleResizing() {
      setResizeState(handleResize());
    }
    window.addEventListener("resize", handleResizing);
  });

  // handleResize()

  return (
    <>
      <div className="w-full mb-2 bg-white shadow-md max-h-32 rounded">
        <div className="flex h-full">
          <div className="md:flex-shrink-0 h-full">
            <img
              className="max-h-32  w-full object-contain"
              src="http://www.hoteldessablesblancs.fr/wa_0_p/pa_15ixvshre48gyz/sl_LOGO_20HOMARD_20REGATE_2040.jpg?1g3y3c2ral8gih"
              alt="La regate"
            />
          </div>
          <div className="p-2 h-full flex flex-col justify-center text-center w-full gap-4">
            <div className="uppercase tracking-wide text-sm  font-semibold">
              Hotel des sables blancs
            </div>
            {/* Partie menu */}
            <div className="bg-white flex justify-center gap-4 lg:gap-32">
              <div>
                <button
                  onClick={() => {
                    dispatch(updatePage({ currentPage: "HOME" }));
                  }}
                  className="py-2 px-4 bg-gray-500 text-white font-semibold rounded-lg shadow-md hover:bg-white hover:text-red-500 focus:outline-none"
                >
                  {resizeState === "icon" ? (
                    <FontAwesomeIcon icon={faHome} />
                  ) : (
                    "Accueil"
                  )}
                </button>
              </div>
              <div>
                <button
                  onClick={() => {
                    dispatch(updatePage({ currentPage: "HOTEL" }));
                  }}
                  className="py-2 px-4 bg-red-500 text-white font-semibold rounded-lg shadow-md hover:bg-white hover:text-red-500 focus:outline-none"
                >
                  {resizeState === "icon" ? (
                    <FontAwesomeIcon icon={faHotel} />
                  ) : (
                    "Hotel"
                  )}
                </button>
              </div>
              <div>
                <ul className="w-full flex">
                  <li className="relative dropdown w-full  cursor-pointer font-bold text-base bg-gray-500 text-white font-semibold rounded-lg shadow-md hover:bg-white hover:text-red-500 focus:outline-none">
                    <button className="py-2 px-4 bg-red-500 text-white font-semibold rounded-lg shadow-md hover:bg-white hover:text-red-500 focus:outline-none">
                      {resizeState === "icon" ? (
                        <FontAwesomeIcon icon={faUtensils} />
                      ) : (
                        "Restaurant"
                      )}

                    </button>
                    <div className="group-hover:block dropdown-menu absolute hidden h-auto w-24 md:w-auto">
                      <ul className="top-0 w-full shadow px-2 py-8 bg-white">
                        <li
                          onClick={() => {
                            dispatch(
                              updatePage({
                                currentPage: "RESTAURANT",
                                subPage: "BASEMENU",
                              })
                            );
                          }}
                          className="py-1 block font-bold text-black-100 hover:bg-gray-100 cursor-pointer"
                        >
                          La Carte
                        </li>
                        <li
                          onClick={() => {
                            dispatch(
                              updatePage({
                                currentPage: "RESTAURANT",
                                subPage: "SEAFOOD",
                              })
                            );
                          }}
                          className="py-1 block text-black-100 hover:bg-gray-100 font-bold cursor-pointer"
                        >
                          Nos plateaux de fruits de mer
                        </li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </div>
              <div>
                <button
                  onClick={() => {
                    dispatch(updatePage({ currentPage: "CONTACT" }));
                  }}
                  className="py-2 px-4 bg-blue-500 text-white font-semibold rounded-lg shadow-md hover:bg-white hover:text-red-500 focus:outline-none"
                >
                    {resizeState === "icon" ? (
                        <FontAwesomeIcon icon={faEnvelopeOpenText} />
                      ) : (
                        "Contact"
                      )}

                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
