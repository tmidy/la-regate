import { Action, configureStore, ThunkAction } from "@reduxjs/toolkit";
import pagereducer from "../store/page/pagereducer";

export const store = configureStore({
  reducer: {
    page: pagereducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<any>
>;
