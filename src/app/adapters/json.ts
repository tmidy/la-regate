import prices from "../../assets/jsons/prices.json";
import weekends from "../../assets/jsons/weekends.json";
import restaurant from "../../assets/jsons/restaurant.json";

type NightStand = {
  bedroom: string;
  comments: string;
};

export type genders = "single" | "couples";

export type NightStandRecord = Record<genders, NightStand>;

type weekendDiscovery = {
  oneNight: NightStandRecord;
  twoNights: NightStandRecord;
};

type pricesAdapterType = {
  bedroom: string;
  extraBed: string;
  pets: string;
  breakfast: string;
  halfBoardSingle: string;
  halfBoardCouples: string;
};

export type pricesKeys = keyof pricesAdapterType;

export type pricesWeekendDiscovery = keyof weekendDiscovery;

const pricesAdpater: Record<string, pricesAdapterType> = prices;

export const WeekendAdapter: Record<string, weekendDiscovery> = weekends;

export const filterNights = (
  nightNumbers: pricesWeekendDiscovery
): Array<NightStandRecord> => {
  return Object.keys(WeekendAdapter).map((key: string) => {
    return WeekendAdapter[key][nightNumbers];
  });
};

export default pricesAdpater;

export type PlateDescription = {
  name: string;
  description?: string;
  price: string;
};

export type MainCourse = {
  seaFood: Array<PlateDescription>;
  meatFood: Array<PlateDescription>;
};

export type RestaurantMenuType = {
  starter: Array<PlateDescription>;
  main: MainCourse;
  dessert: Array<PlateDescription>;
};

export type seaFoodMenu = {
  soloDuos: Array<PlateDescription>;
  onThePlate: Array<PlateDescription>;
};

export type RestaurantType = {
  baseMenu: RestaurantMenuType;
  seaFoodMenu: seaFoodMenu;
};

export const RestaurantAdapter: RestaurantType = restaurant;